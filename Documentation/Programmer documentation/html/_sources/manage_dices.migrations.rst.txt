manage\_dices.migrations package
================================

Submodules
----------

manage\_dices.migrations.0001\_initial module
---------------------------------------------

.. automodule:: manage_dices.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

manage\_dices.migrations.0002\_cubes\_preset\_owner module
----------------------------------------------------------

.. automodule:: manage_dices.migrations.0002_cubes_preset_owner
    :members:
    :undoc-members:
    :show-inheritance:

manage\_dices.migrations.0003\_auto\_20200429\_2303 module
----------------------------------------------------------

.. automodule:: manage_dices.migrations.0003_auto_20200429_2303
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: manage_dices.migrations
    :members:
    :undoc-members:
    :show-inheritance:
