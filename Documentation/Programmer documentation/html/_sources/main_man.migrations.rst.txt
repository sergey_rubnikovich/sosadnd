main\_man.migrations package
============================

Submodules
----------

main\_man.migrations.0001\_initial module
-----------------------------------------

.. automodule:: main_man.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

main\_man.migrations.0002\_delete\_user module
----------------------------------------------

.. automodule:: main_man.migrations.0002_delete_user
    :members:
    :undoc-members:
    :show-inheritance:

main\_man.migrations.0003\_userlikes module
-------------------------------------------

.. automodule:: main_man.migrations.0003_userlikes
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: main_man.migrations
    :members:
    :undoc-members:
    :show-inheritance:
