from django.urls import include, path

from . import views

app_name = 'manage_lists'

extra_global_collection_patterns = [
    path('get_pdf/<str:list_uuid>/', views.get_pdf, name='get_pdf'),
    path('like/', views.like_hero, name='like_hero'),
    path('add/', views.add_hero, name='add_hero'),
    path('delete/', views.delete_global_hero, name='delete_global_hero'),
    path('', views.see_global_collection, name='collection'),
]

extra_locale_collection_patterns = [
    path('get_pdf/<str:list_uuid>/', views.get_pdf, name='get_pdf'),
    path('delete/', views.delete_local_hero, name='delete_local_hero'),
    path('edit/<str:list_uuid>/', views.edit_hero, name='edit_hero'),
    path('publish/', views.publish_hero, name='publish_hero'),
    path('', views.see_locale_collection, name='locale_collection'),
]

urlpatterns = [
    path('collection/', include(extra_global_collection_patterns)),
    path('my_heroes/', include(extra_locale_collection_patterns)),
    path('new_hero/', views.create_hero, name='create_hero'),
    path('save_hero/', views.save_created_hero, name='save_hero'),
    path('', views.create_hero, name='create_hero'),
]
