from django.test import TestCase
from SOSADnD import settings


class SettingsReadyForProduct(TestCase):
    def test_is_debug_false(self):
        self.assertEqual(settings.DEBUG, False)
