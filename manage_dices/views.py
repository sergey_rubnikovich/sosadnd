import json

from django.contrib.auth.decorators import login_required
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView

from users.models import CustomUser
from .models import Cubes


@login_required
def add_preset(request):
    """
    Function adds new preset-configuration in database Cubes and save it.
    :param request: HTTP request
    :return: JsonResponse object
    """
    if not (request.method == "POST"):
        raise Http404
    new_preset = Cubes(user=request.user)
    raw_preset_config = json.loads(request.POST.get("preset", None))
    new_preset.set_preset_values(raw_preset_config)
    if not new_preset.is_preset_valid():
        return JsonResponse({"dice_error": "Вы ввели некорректное значение!\nВыставьте другие значения."})

    if Cubes.objects.does_preset_exist(
            user=new_preset.user,
            d4=new_preset.d4,
            d6=new_preset.d6,
            d8=new_preset.d8,
            d10=new_preset.d10,
            d12=new_preset.d12,
            d20=new_preset.d20
    ):
        return JsonResponse({"dice_error": "Данная конфигурация уже существует!\nВыставьте другие значения."})
    new_preset.save()
    return JsonResponse({})


@login_required
def delete_preset(request):
    """
    Function removes the existing preset-configuration in database.
    :param request: Http request
    :return: redirect to /dice/
    """
    cube_uuid = json.loads(request.POST.get("cube_uuid", None))
    if cube_uuid is None:
        raise Http404
    get_object_or_404(Cubes, user=request.user, cube_uuid=cube_uuid).delete()
    return JsonResponse({})


def get_rolling_result(request):
    """
    Function returns rolling result.
    :param request: HTTP request
    :return: JsonResponse object
    """
    # return roll for anonym-user
    if not request.user.is_authenticated:
        return roll_unsaved_preset(request, CustomUser(username="Anonymous"))

    # if there is a saved-preset - roll it, else roll unsaved preset
    cube_uuid = request.POST.get("cube_uuid", None)
    if not cube_uuid:
        return roll_unsaved_preset(request, request.user)
    calculated_preset = get_object_or_404(Cubes, user=request.user, cube_uuid=json.loads(cube_uuid))
    return JsonResponse({"result": calculated_preset.eval_rolling_result()})


def roll_unsaved_preset(request, user):
    new_preset = Cubes(user=user)
    raw_preset_config = request.POST.get("preset", None)
    if not raw_preset_config:
        return JsonResponse({"dice_error": "Вы ввели некорректное значение!\nВыставьте другие значения."})
    raw_preset_config_serialised = json.loads(raw_preset_config)
    new_preset.set_preset_values(raw_preset_config_serialised)
    if new_preset.is_preset_valid():
        return JsonResponse({"result": new_preset.eval_rolling_result()})
    return JsonResponse({"dice_error": "Вы ввели некорректное значение!\nВыставьте другие значения."})


class DicePageView(ListView):
    context_object_name = 'presets'
    template_name = 'manage_dices/Dops.html'

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Cubes.objects.personal_presets(user=self.request.user)
        return []

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(DicePageView, self).get_context_data(**kwargs)
        context['DICES_TYPES'] = Cubes.DICES_TYPES
        return context
