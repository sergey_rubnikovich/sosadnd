from django.urls import path
from . import views

app_name = 'manage_dices'

urlpatterns = [
    path('add/', views.add_preset, name='save_preset'),
    path('delete_preset/', views.delete_preset, name='delete_preset'),
    path('roll/', views.get_rolling_result, name='inreg_roll_dices'),
    path('', views.DicePageView.as_view(), name='roll_page'),
]
