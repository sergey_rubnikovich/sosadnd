from django.apps import AppConfig


class ManageDicesConfig(AppConfig):
    name = 'manage_dices'
