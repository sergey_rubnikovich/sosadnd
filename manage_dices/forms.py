from django.forms import ModelForm
from . import models


class PresetForm(ModelForm):
    class Meta:
        model = models.Cubes
        fields = ['d4', 'd6', 'd8', 'd10', 'd12', 'd20']
