import random
import uuid

from django.conf import settings
from django.db import models


def str_to_int(input_str=None):
    """
    Function transforms string to int-type.
    :param input_str: input-string
    :return: number or 0 if string is wrong
    """
    try:
        return int(input_str)
    except TypeError:
        return 0
    except ValueError:
        return 0


class CubesQuerySet(models.QuerySet):
    def personal_presets(self, user):
        return self.filter(user=user)


class CubesManager(models.Manager):
    def get_queryset(self):
        return CubesQuerySet(self.model, using=self._db)

    def personal_presets(self, user):
        return self.get_queryset().personal_presets(user)

    def does_preset_exist(self, user, **preset):
        return self.filter(
            user=user,
            d4=preset['d4'],
            d6=preset['d6'],
            d8=preset['d8'],
            d10=preset['d10'],
            d12=preset['d12'],
            d20=preset['d20'],
        ).exists()


class Cubes(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True,
                             verbose_name='preset owner')
    d4 = models.SmallIntegerField(default=0)
    d6 = models.SmallIntegerField(default=0)
    d8 = models.SmallIntegerField(default=0)
    d10 = models.SmallIntegerField(default=0)
    d12 = models.SmallIntegerField(default=0)
    d20 = models.SmallIntegerField(default=0)
    cube_uuid = models.UUIDField(unique=True, default=uuid.uuid4, blank=False)
    objects = CubesManager()

    ACCEPTABLE_DICES_FIELDS = ['d4', 'd6', 'd8', 'd10', 'd12', 'd20']
    DICES_TYPES = [4, 6, 8, 10, 12, 20]
    MAX_PRESET_SUM = 999

    def set_preset_values(self, preset_config):
        """
        Function sets values from raw configuration to fields.
        :param preset_config: raw preset configuration (list)
        :return: if preset configuration is None raises ValueError,
        return True if fields filled successfully
        """
        if preset_config is None:
            raise ValueError('Preset configuration is empty!')
        # change all non-int type data to 0
        preset_config = self.clean_data(preset_config)
        # set values in model fields
        self.d4 = preset_config['d4']
        self.d6 = preset_config['d6']
        self.d8 = preset_config['d8']
        self.d10 = preset_config['d10']
        self.d12 = preset_config['d12']
        self.d20 = preset_config['d20']

    def eval_rolling_result(self):
        """
        Functions returns random integer value based on preset's configuration.
        :return: int number
        """
        result_sum = 0
        dice_type = self.DICES_TYPES
        for (index, t_dice) in enumerate(self.ACCEPTABLE_DICES_FIELDS):
            for _ in range(self.__getattribute__(t_dice)):
                result_sum += random.randint(1, dice_type[index])
        return result_sum

    def is_preset_valid(self):
        """
        Function checks preset has permissible values.
        Preset must contain positive numbers and be less than MAX_PRESET_SUM (const).
        :return: boolean value shows if preset allowable to use
        """
        max_result_sum = 0
        for (index, t_dice) in enumerate(self.ACCEPTABLE_DICES_FIELDS):
            dice_val = self.__getattribute__(t_dice)
            if dice_val < 0:
                return False
            max_result_sum += self.DICES_TYPES[index] * dice_val

        return 0 < max_result_sum < self.MAX_PRESET_SUM

    @staticmethod
    def clean_data(preset_config):
        """
        Function cleans preset's configuration using str_to_int function.
        :param preset_config: raw preset configuration
        :return: cleaned preset configuration
        """
        for (t_dice, dice_val) in preset_config.items():
            preset_config[t_dice] = str_to_int(dice_val)
        return preset_config

    def __repr__(self):
        return f"Cubes {self.cube_uuid}"

    def __str__(self):
        return f"{self.user}:{self.d4}./.{self.d6}./.{self.d8}./.{self.d10}./.{self.d12}./.{self.d20}"

    class Meta:
        ordering = ['user']
