# SOSADND

[Сайт-ассистент для DnD](https://sosadnd.pythonanywhere.com/ "Перейти на сайт")


## Главная страница
![main page](./example_pictures/main.png)

## Общедоступная коллекция персонажей
![dices page](./example_pictures/heroes.png)


## Страница бросков кубика
![dices page](./example_pictures/dices.png)

## Элементы создания нового персонажа
![dices page](./example_pictures/new_1.png)

![dices page](./example_pictures/new_2.png)

![dices page](./example_pictures/new_3.png)
