from django.core.exceptions import ValidationError as CoreValidationError, ObjectDoesNotExist
from django.contrib.auth.hashers import is_password_usable
from django.forms import CharField, Form, PasswordInput, ValidationError
from django.utils.translation import ugettext_lazy as _

from users.models import CustomUser


class LoginForm(Form):
    username = CharField(max_length=30)
    password = CharField(max_length=32, widget=PasswordInput)

    def clean_username(self):
        username = self.cleaned_data['username']

        if len(username) > 30:
            raise ValidationError(_('Username is too long - username must be contain no more than 30 symbols'))
        if not username:
            raise ValidationError(_('Empty username - username must be specified'))
        return username


class RegisterForm(Form):
    new_username = CharField(max_length=30)
    psw_1 = CharField(max_length=32, widget=PasswordInput)
    psw_2 = CharField(max_length=32, widget=PasswordInput)

    def clean_new_username(self):
        new_user_username = self.cleaned_data['new_username']

        if not new_user_username:
            raise ValidationError(_('Empty username - username must be specified'))
        if CustomUser.objects.filter(username=new_user_username).exists():
            raise ValidationError(_('Existing username - username must be unique'))
        if len(new_user_username) > 30:
            raise ValidationError(_('Username is too long - username must be contain no more than 30 symbols'))
        return new_user_username

    def clean(self):
        cleaned_data = super().clean()
        psw_1 = cleaned_data.get("psw_1")
        psw_2 = cleaned_data.get("psw_2")

        if len(psw_1) != len(psw_2) or psw_1 != psw_2:
            raise ValidationError(_('Wrong repetition - passwords don\'t match'))
        if len(psw_1) < 6 or len(psw_2) < 6:
            raise ValidationError(_('Short password - password must be at least six symbols'))
        if not is_password_usable(psw_1):
            raise ValidationError(_('Password unusable - think up more complex password'))


def validate_email(email):
    if email and '@' not in email:
        raise CoreValidationError(_(f'{email} is incorrect email'),
                                  params={'email': email},)


class AccountSettingsForm(Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        super(AccountSettingsForm, self).__init__(*args, **kwargs)

    username = CharField(max_length=30, required=False)
    email = CharField(max_length=80, required=False, validators=[validate_email])
    psw_1 = CharField(max_length=32, widget=PasswordInput, required=False)
    psw_2 = CharField(max_length=32, widget=PasswordInput, required=False)

    def clean_username(self):
        username = self.cleaned_data['username']
        # if username does not specified - keep current username
        if not username:
            return username

        try:
            # if there is other with specified username - raise it, else - pass
            user = CustomUser.objects.get(username=username)
            if user.id != self.user.id:
                raise ValidationError(_('Existing username - username must be unique'))
        except ObjectDoesNotExist:
            pass

        if len(username) > 30:
            raise ValidationError(_('Username is too long - username must be contain no more than 30 symbols'))
        return username

    def clean(self):
        cleaned_data = super().clean()
        psw_1 = cleaned_data.get("psw_1")
        psw_2 = cleaned_data.get("psw_2")
        if not psw_1 or not psw_2:
            return
        if psw_1 != psw_2:
            raise ValidationError(_('Wrong repetition - passwords don\'t match'))
        if len(psw_1) < 6 or len(psw_2) < 6:
            raise ValidationError(_('Short password - password must be at least six symbols'))
        if not is_password_usable(psw_1):
            raise ValidationError(_('Password unusable - think up more complex password'))
