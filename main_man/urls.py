from django.conf import settings
from django.contrib.auth.views import LogoutView
from django.urls import include, path
from . import views

app_name = 'main_man'

extra_settings = [
    path('delete/', views.DeleteUserView.as_view(), name='delete_account'),
    path('update/', views.UpdateAccountView.as_view(), name='update_settings'),
    path('', views.SettingsView.as_view(), name='account_settings'),
]

urlpatterns = [
    path('reset', views.ResetPasswordPageView.as_view(), name='reset_psw'),
    path('register/', views.RegisterView.as_view(), name='register'),
    path('login/', views.MyLoginView.as_view(), name='login'),
    path('user/', views.AccountView.as_view(), name='account_page'),
    path('settings/', include(extra_settings)),
    path('user/logout/', LogoutView.as_view(next_page=settings.LOGOUT_REDIRECT_URL), name='logout'),
    path('', views.IndexView.as_view(), name='index_page'),
]
